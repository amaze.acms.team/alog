package org.acms.alog.service;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.acms.alog.dao.connectToDB;
import org.acms.alog.entity.AlogInputRequest;
import org.acms.alog.entity.Recording;
import org.acms.alog.entity.User;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;

public class AlogService {
	connectToDB conn = new connectToDB();

	public String saveRecording(AlogInputRequest request) throws Exception {

		User user = getRecordingById(request.getUserID());

		Recording recording = populateRecording(request);
		if (user == null) {
			user = new User();
			user.setUserId(request.getUserID());
			List<Recording> list = new ArrayList<>();
			list.add(recording);
			user.setRecordingsList(list);
		} else {
			List<Recording> list = user.getRecordingsList();
			list.add(recording);
			user.setRecordingsList(list);
		}
		conn.saveRecording(user);
		recording.getAudio().uploadFrom(convertStreamToByte(request.getRecord()));

		return "record added";

	}

	private Recording populateRecording(AlogInputRequest request) throws IOException {
		Date date = new Date();
		long time = date.getTime();
		Timestamp ts = new Timestamp(time);
		Recording recording = new Recording();
		recording.setRecordingName(request.getRecordName());
		String a[] = (ts.toString()).split("[^0-9]+");
		String s = "";
		for (int i = 0; i < a.length; i++) {
			s += a[i];
		}
		recording.setRecordingId(s);

		DynamoDBMapper mapper = conn.getMapper();
		recording.setAudio(mapper.createS3Link("alogacms", recording.getRecordingId()));
		return recording;

	}

	public byte[] convertStreamToByte(InputStream fis) throws IOException {
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		byte[] b = new byte[1024];

		for (int readNum; (readNum = fis.read(b)) != -1;) {
			bos.write(b, 0, readNum);
		}

		byte[] bytes = bos.toByteArray();
		return bytes;
	}

	public User getRecordingById(int id) throws Exception {

		return conn.getRecordingById(id);

	}

	public Recording getRecordingByRecordingId(int id, String recordingId) throws Exception {

		User userrecordings = getRecordingById(id);
		List<Recording> recordingsList = userrecordings.getRecordingsList();
		for (Recording r : recordingsList) {
			if (r.getRecordingId().equals(recordingId)) {
				return r;
			}
		}
		return null;

	}
}
