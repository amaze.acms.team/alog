package org.acms.alog.resources;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.acms.alog.entity.AlogInputRequest;
import org.acms.alog.service.AlogService;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

@Path("/rec")
public class AlogController {
	AlogService alogservice=new AlogService();
	ObjectMapper objectMapper = new ObjectMapper().disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	@Path("/postrecording")
	public String postRecording(AlogInputRequest request) throws Exception {
		return alogservice.saveRecording(request);
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getrecording/{id}")
	public String getRecording(@PathParam("id") int id) throws Exception {
		return objectMapper.writeValueAsString(alogservice.getRecordingById (id));
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getrecording/{id}/{recordingId}")
	public String getRecording(@PathParam("id") int id,@PathParam("recordingId") String recordingId) throws Exception {
		return objectMapper.writeValueAsString(alogservice.getRecordingByRecordingId(id,recordingId));

	}
}
