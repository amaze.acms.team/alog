package org.acms.alog.entity;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBDocument;
import com.amazonaws.services.dynamodbv2.datamodeling.S3Link;


@DynamoDBDocument
public class Recording {
	private String recordingId;
	private String recordingName;
	//private InputStream record;
	private  S3Link audio;

	
	public Recording() {
	super();
	}

	
	
	@DynamoDBAttribute(attributeName = "recordingName")
	public String getRecordingName() {
		return recordingName;
	}

	public void setRecordingName(String recordingName) {
		this.recordingName = recordingName;
	}
  
	@DynamoDBAttribute(attributeName = "RecordingId")
	public String getRecordingId() {
		return recordingId;
	}

	public void setRecordingId(String recordingId) {
		this.recordingId = recordingId;
	}

	
	public S3Link getAudio() {
		return audio;
	}

	public void setAudio(S3Link audio) {
		this.audio = audio;
	}


	

}
