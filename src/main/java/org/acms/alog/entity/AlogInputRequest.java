package org.acms.alog.entity;

import java.io.InputStream;

public class AlogInputRequest {

	private int userID;
	private InputStream record;
	private String recordName;

	
	public String getRecordName() {
		return recordName;
	}

	public void setRecordName(String recordName) {
		this.recordName = recordName;
	}

	public InputStream getRecord() {
		return record;
	}

	public void setRecord(InputStream record) {
		this.record = record;
	}

	public int getUserID() {
		return userID;
	}

	public void setUserID(int userID) {
		this.userID = userID;
	}

}
