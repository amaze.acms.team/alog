package org.acms.alog.entity;

import java.util.List;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
@DynamoDBTable(tableName="user")
public class User {
	private int userId;
    private List<Recording> recordingsList;
    
	public User() {
		super();
	}

	@DynamoDBHashKey(attributeName = "useId")
	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}
	@DynamoDBAttribute(attributeName="recordingsList")
	public List<Recording> getRecordingsList() {
		return recordingsList;
	}

	public void setRecordingsList(List<Recording> recordingsList) {
		this.recordingsList = recordingsList;
	}
	

}
